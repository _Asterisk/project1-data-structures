#include "deque.h"
#include <stdio.h>
#include <cassert>

#ifndef INIT_CAP
#define INIT_CAP 4
#endif

deque::deque()
{
	this->cap = INIT_CAP;
	this->data = new int[cap];
	this->front_index = 0;
	this->next_back_index = 0;
}

deque::deque(int* first, int* last)
{
	if (last <= first) return;
	this->cap = last - first + 1;
	this->data = new int[this->cap];
	this->front_index = 0;
	this->next_back_index = this->cap-1;
	for (size_t i = 0; i < this->cap-1; i++) {
		this->data[i] = first[i];
	}
}

deque::deque(const deque& D)
{
	this->cap = this->next_back_index = D.cap;
	this-> data = new int[this->cap];
	for (size_t i = 0; i < this-> cap-1 ; i++){
		 this->data[i] = D[i];
		}

}

deque::deque(deque&& D)
{
	this->data = D.data;
	this->front_index = D.front_index;
	this->next_back_index = D.next_back_index;
	this->cap = D.cap;
	D.data = NULL;
}

deque& deque::operator=(deque RHS)
{
	if(this == &RHS) return (*this);

	delete[] this-> data;

	this->cap = RHS.cap;
	this->data = new int[this->cap];
	for(size_t i =  0; i < this->cap; i++){
		this->data[i] = RHS.data[i];
		}
	return *this;
}

deque::~deque()
{
	if (this->data != NULL) delete[] this->data;
}

int deque::back() const
{
	assert(!empty());
	if (next_back_index == 0){
		return data[next_back_index - 1];
		}
	return data[next_back_index];
}

int deque::front() const
{
	assert(!empty());

	return data[front_index];
}

size_t deque::capacity() const
{
	return this-> cap - 1;
}

int& deque::operator[](size_t i) const
{
	assert(i < this->size());
	return this->data[(front_index + i) % this->cap];
}

size_t deque::size() const
{
	if (front_index <= next_back_index){

		return  next_back_index - front_index;

	}
	else{
		return cap + next_back_index - front_index ;
	}
}

bool deque::empty() const
{
	return (next_back_index == front_index);
}

bool deque::needs_realloc()
{
	return ((next_back_index + 1) % cap == front_index);
}

void deque::push_front(int x)
{
	if (needs_realloc() == true){
			int* newdata = new int [cap*2];
			for (size_t i = 0; i < cap - 1 ; i++){
				newdata[i] = data[i];
				}
			delete[] data;
			data = newdata;
			cap *= 2;
		}
	if (needs_realloc() == false){
		size_t a = (this->front_index + cap - 1) % (cap);
		this->front_index = a;
		this->data[a] = x;
	}
}

void deque::push_back(int x)
{
	if (needs_realloc() == true) {
		int* newdata = new int[cap*2];
		for (size_t i = 0; i < cap; i++){
				newdata[i] = data[i];
			}
			delete [] data;
			data = newdata;
			cap *= 2;
		}

		size_t a =  (this->next_back_index + 1) % cap;
		this->data[next_back_index] = x;
		this->next_back_index = a;

}

int deque::pop_front(){
	if (front_index == next_back_index) {
			front_index = next_back_index;
			return front_index;
	}
	else {
		front_index = front_index + 1;
		return front_index;
		}
}

int deque::pop_back()
{
	if (cap == next_back_index) {
		front_index = next_back_index;
		return front_index;
		}
	else {
		next_back_index = next_back_index - 1;
		return next_back_index;
		}
}

void deque::clear()
{
	this->front_index = 0;
	this->next_back_index = 0;
}

void deque::print(FILE* f) const
{
	for(size_t i=this->front_index; i!=this->next_back_index;
			i=(i+1) % this->cap)
		fprintf(f, "%i ",this->data[i]);
	fprintf(f, "\n");
}
