#include "deque.h"
#include <stdio.h>
#include <cassert>

#ifndef INIT_CAP
#define INIT_CAP 4
#endif

deque::deque()
{
	this->cap = INIT_CAP;
	this->data = new int[cap];
	this->front_index = 0;
	this->next_back_index = 0;
}

deque::deque(int* first, int* last)
{
	if (last <= first) return;
	this->cap = last - first + 1;
	this->data = new int[this->cap];
	this->front_index = 0;
	this->next_back_index = this->cap-1;
	for (size_t i = 0; i < this->cap-1; i++) {
		this->data[i] = first[i];
	}
}

deque::deque(const deque& D)
{
	this->front_index = this->cap = D.front_index;
	this->next_back_index = this->cap = D.next_back_index;
	for (size_t i = 0; i < this->front_index ; i++){
			this->data[i] = D.data[i];
		}
	for (size_t i = 0;  i < this->next_back_index; i++){
			this->data[i] = D.data[i];
		}
}

deque::deque(deque&& D)
{
	this->data = D.data;
	this->front_index = D.front_index;
	this->next_back_index = D.next_back_index;
	this->cap = D.cap;
	D.data = NULL;
}

deque& deque::operator=(deque RHS)
{
	int* x = this->data;
	this->data = RHS.data;
	RHS.data = x;

	this->front_index = RHS.front_index;
	this->next_back_index = RHS.next_back_index;
	this->cap = RHS.cap;
	return *this;
}

deque::~deque()
{
	if (this->data != NULL) delete[] this->data;
}

int deque::back() const
{
	assert(!empty());
	if (next_back_index == 0){
		return data[cap - 1];
		}
	return data[next_back_index - 1];
}

int deque::front() const
{
	assert(!empty());
	return data[front_index];
}

size_t deque::capacity() const
{
	return this-> cap - 1;
}

int& deque::operator[](size_t i) const
{
	assert(i < this->size());
	return this->data[(front_index + i) % this->cap];
}

size_t deque::size() const
{
	if (front_index <= next_back_index){
		return next_back_index - front_index;
	}
	return cap + next_back_index - front_index;
}

bool deque::empty() const
{
	return (next_back_index == front_index);
}

bool deque::needs_realloc()
{
	return ((next_back_index + 1) % cap == front_index);
}

void deque::push_front(int x)
{
	if (needs_realloc() == true){
			int* newdata = new int [cap*2];
			for (size_t i = 0; i < cap - 1 ; i++){
				newdata[i] = data[i];
				}
			delete[] data;
			data = newdata;
			cap *= 2;
		}
		size_t reduced = ((this->front_index -1) + this->cap) %(this->cap);
		this->data[reduced] = x;
		this->front_index = reduced;
}

void deque::push_back(int x)
{
	if (needs_realloc() == true) {
		int* newdata = new int[cap*2];
		for (size_t i =0; i < next_back_index; i++){
				newdata[i] = data[i];
			}
			delete [] data;
			data = newdata;
			cap *= 2;
		}
		size_t reduced =  (this->next_back_index + 1) % cap;
		this->data[this->next_back_index] = x;
		this->next_back_index = reduced;
}

int deque::pop_front(){
	if (front_index == next_back_index) {
		front_index = next_back_index;
		return front_index;
		}
	else {
		front_index = front_index + 1;
		}
}

int deque::pop_back()
{
	if (front_index == next_back_index) {
		front_index = next_back_index;
		}
	else {
		next_back_index = next_back_index - 1;
		}
}

void deque::clear()
{
	this->front_index = 0;
	this->next_back_index = 0;

}

void deque::print(FILE* f) const
{
	for(size_t i=this->front_index; i!=this->next_back_index;
			i=(i+1) % this->cap)
		fprintf(f, "%i ",this->data[i]);
	fprintf(f, "\n");
}
